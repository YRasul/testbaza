import psycopg2
from config import db_host, db_user, db_password, db_name


def testConnect():
    """ Connect to the PostgreSQL database server """
    conn = None
    try:
        con = psycopg2.connect(
            host=db_host,
            user=db_user,
            password=db_password,
            database=db_name
        )
        with con.cursor() as cur:
            cur.execute(
                "SELECT version();"
            )
            return(f"Server version: {cur.fetchone()}")

    except Exception as _ex:
        print("[INFO] Error while working with PostgreSQL", _ex)
    finally:
        if conn is not None:
            conn.close()
            print('Database connection closed.')
            return('Database connection closed.')

if __name__ == "__main__":
    a = testConnect()
    print(a)
