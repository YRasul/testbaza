import psycopg2
from config import db_host, db_user, db_password, db_name
from psycopg2 import Error

connection = None
try:
    # Подключиться к существующей базе данных
    connection = psycopg2.connect(user=db_user,
                                  password=db_password,
                                  host=db_host,
                                  database=db_name)

    # Создайте курсор для выполнения операций с базой данных
    cursor = connection.cursor()
    # SQL-запрос для создания новой таблицы
    create_table_query = '''CREATE TABLE Marka
                          (ID INT PRIMARY KEY     NOT NULL,
                          MODEL           TEXT    NOT NULL,
                          PRICE         REAL); '''
    # Выполнение команды: это создает новую таблицу
    cursor.execute(create_table_query)
    connection.commit()
    print("Таблица успешно создана в PostgreSQL")

except (Exception, Error) as error:
    print("Ошибка при работе с PostgreSQL", error)
finally:
    if connection is not None:
        cursor.close()
        connection.close()
        print("Соединение с PostgreSQL закрыто")